# DMFSU #

## The official repository for the 2015 - 2016 Dance Marathon Tech Team ##

* Version 1.0

### How do I get set up? ###

* Create a Bitbucket account to add and make changes to the source code!
* Click the "Source" button on the left hand side of the screen to view each section's respective source code.  All sections are labeled appropriately.
* **Note that your contribution will not be uploaded to the dmfsu.org website until all changes are finalized.**


### Contribution guidelines ###

* Open your newly created folder and navigate to the folder that you want to make changes to.
* Edit the index.html file located inside of the section you would like to make changes to with any text editor (I recommend using Sublime Text http://www.sublimetext.com OR Komodo edit http://komodoide.com/komodo-edit/ ) 
* When you are finished, go back to SourceTree.  You will see all of the changes you made highlighted in the “Unstaged Files” section of the application


1. Hover over the files in the “Unstaged Files” section that you would like to have uploaded to Bitbucket and click the ADD button. This will add the selected file to the “staging area”, preparing it to be uploaded. 
2. Click the COMMIT button.  This will commit your changes to the project’s history.
3. When you are ready to upload your files, click the PUSH button.

### File locations and naming conventions ###
* **All HTML files are to be named index.html and placed in a folder with a short name that describes the section (such as "sponsors", "fundraising", etc)**
* **All images are to be placed inside of the "images" folder**
* **All JavaScript files are to be placed inside of the "js" folder**
* **No files can contain a space in their filename**
### Who do I talk to? ###

* Repo owner: Andrew Pendergast ( amp12f@my.fsu.edu )